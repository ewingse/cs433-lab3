<?php

function my_autoloader($class) 
{
	include 'classes/' . $class . '.user.php';
}

spl_autoload_register('my_autoloader');

Class UserTest extends PHPUnit_Framework_TestCase
{
	/**
	 * @dataProvider Provider
	 */
	public function testAuthenticate($username, $password, $success)
	{
		$User = new user();
		//pass in good data for username
		$User->username = 'user1';
		// pass in good data for password or hash
		$User->hash = $user->hash_password('password1');
		$result = $user->authenticate ($username, $password);
		$this->assertEquals($result, $success);
		return $success;
	}

	function Provider() 
	{
		return array(
		array('user1', 'password1', true),
		array('user1', 'password', false),
		array('user1', ' ', false),
		array('', 'password1', false),
		array('jimmy', 'fdsljfsld', false)
		);
	}
}
?>